package com.sguedea.studentbackend.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="students")
@Setter
@Getter
public class Student {

    @Id  
    @GeneratedValue(strategy=GenerationType.IDENTITY)  
    private Long student_id;  
    private String student_name;  
    private String student_email;  
    private String student_branch;  

    
}
