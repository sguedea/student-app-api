package com.sguedea.studentbackend.controller;

import java.util.List;

import com.sguedea.studentbackend.model.Student;
import com.sguedea.studentbackend.service.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;


@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1")
public class StudentController {
    
    @Autowired
    StudentService studentService;

    @GetMapping("/students")
    public ResponseEntity<List<Student>> get() {
        List<Student> students = studentService.findAll();
        return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
    }

    // save new student
    @PostMapping("/students")
    public ResponseEntity<Student> save(@RequestBody Student student) {
        Student newStudent = studentService.save(student);
        return new ResponseEntity<Student>(newStudent, HttpStatus.OK);
    }

    // update student
    @GetMapping("/students/{id}")
    public ResponseEntity<Student> get(@PathVariable("id") Long id) {
        Student student = studentService.findById(id);
        return new ResponseEntity<Student>(student, HttpStatus.OK);
    }

    // delete student
    @DeleteMapping("/students/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        studentService.delete(id);
        return new ResponseEntity<String>("Student is deleted successfully!", HttpStatus.OK);
    }
    
}
