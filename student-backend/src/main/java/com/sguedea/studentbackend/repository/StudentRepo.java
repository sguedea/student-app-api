package com.sguedea.studentbackend.repository;

import com.sguedea.studentbackend.model.Student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepo extends JpaRepository<Student, Long> {
    
}
