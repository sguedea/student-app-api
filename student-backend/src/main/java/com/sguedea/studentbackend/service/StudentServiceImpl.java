package com.sguedea.studentbackend.service;

import java.util.List;

import com.sguedea.studentbackend.model.Student;
import com.sguedea.studentbackend.repository.StudentRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements StudentService {
    
    @Autowired
    StudentRepo studentRepo;

    @Override
    public List<Student> findAll() {
        return studentRepo.findAll();
    }

    // save record
    @Override
    public Student save(Student student) {
        studentRepo.save(student);
        return student;
    }

    // update record
    @Override
    public Student findById(Long id) {
        if (studentRepo.findById(id).isPresent()) {
            return studentRepo.findById(id).get();
        }
        return null;
    }

    // delete record
    @Override
    public void delete(Long id) {
        Student student = findById(id);
        studentRepo.delete(student);
    }
}
