package com.sguedea.studentbackend.service;

import java.util.List;

import com.sguedea.studentbackend.model.Student;

public interface StudentService {
    List<Student> findAll();

    // save record
    Student save(Student student);

    // update record
    Student findById(Long id);

    // delete 
    void delete(Long id);
}
